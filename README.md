# J2 REST API
This is a module which expose Odoo as a REST API 


## Installing
* Download this module and put into your Odoo addons or custom_addons directory
* Update Apps List and install `J2 REST API` module

# Usage

# 1. post new material
`POST /api/j2_api.materials`

	Request Body

	```json
	{
		"params": {
				"data": {
					"supplier_id": 3,          		// get id from GET /api/j2_api.suppliers
					"material_type": "cotton", 		// options is: 'fabric', 'jeans', and 'cotton'
					"material_code": "CLN-002",
					"material_name": "Bahan Celana",
					"material_buy_price": 15000		// can't be less than 100
				}
		}
	}	
	```

	Response

	```json
	{
		"jsonrpc": "2.0",
		"id": null,
		"result": 1
	}
	```

	The number on `result` is the `id` of the newly created record.


# 2. update material by id
`PUT /api/j2_api.materials/{id}`

	Request Body

	```json
	{
		"params": {
			"data": {
				"supplier_id": 3,
				"material_type": "cotton",
				"material_code": "CLN-002",
				"material_name": "Bahan Celana",
				"material_buy_price": 15000
			}
		}
	}	
	```

	Response

	```json
	{
		"jsonrpc": "2.0",
		"id": null,
		"result": true
	}
	```

	If the result is true it means success and if false or otherwise it means there was an error during update.


# 3. delete material by id
`DELETE /api/j2_api.materials/{id}`

	Response

	```json
	{
		"result": true
	}
	```
    
	If the result is true it means success and if false or otherwise it means there was an error during deletion.


# 4. get supplier list
`GET /api/j2_api.suppliers`

	Response

	```json
	{
		"data": [
			{
				"id": 1,
				"name": "PT Suryaraya Rubberindo Industries"
			},
			{
				"id": 2,
				"name": "PT Wawan Setyawans Industries"
			}
		]
	}
	```

# 5. get material list
`GET /api/j2_api.materials`

	Response

	```json
	{
		"data": [
			{
				"id": 1,
				"name": "Spidol Warna Gelap",
				"code": "ATK-001",
				"type": "fabric",
				"price": 1800
			},
			{
				"id": 8,
				"name": "Atasan Bawahan",
				"code": "ATB-001",
				"type": "jeans",
				"price": 100
			},
			{
				"id": 9,
				"name": "Celana Cutbray",
				"code": "CLN-001",
				"type": "cotton",
				"price": 10000
			},
			{
				"id": 10,
				"name": "Celana Panjang",
				"code": "CLN-002",
				"type": "cotton",
				"price": 15000
			}
		]
	}
	```

# 6. get material list by type
`GET /api/j2_api.materials/{type}` 

	Response

	```json
	{
		"data": [
			{
				"id": 9,
				"name": "Celana Cutbray",
				"code": "CLN-001",
				"type": "cotton",
				"price": 10000
			},
			{
				"id": 10,
				"name": "Celana Panjang",
				"code": "CLN-002",
				"type": "cotton",
				"price": 15000
			}
		]
	}
	```