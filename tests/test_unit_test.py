import unittest

from odoo.tests import common
from odoo.exceptions import Warning

class TestModule(common.TransactionCase):

		def setUp(self):
				super(TestModule, self).setUp()


		def test_data(self):
				#create new supplier
				supplier = self.env['j2_api.suppliers'].create({
						'supplier_name': 'PT Suryaraya Rubberindo Industries'
				})

				supplier_1 = self.env['j2_api.suppliers'].create({
						'supplier_name': 'PT Starks Industries'
				})

				self.assertEqual(supplier, supplier_1)
				print('Test is successful')

				self.assertNotEqual(supplier, supplier_1)
				print('Test is successful')
