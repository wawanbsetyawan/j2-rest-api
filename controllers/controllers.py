import json

from odoo import http, exceptions
from odoo.http import route, request

# error exception for delete
def error_response(error, msg):
		return {
				"jsonrpc": "2.0",
				"id": None,
				"error": {
						"code": 200,
						"message": msg,
						"data": {
								"name": str(error),
								"debug": "",
								"message": msg,
								"arguments": list(error.args),
								"exception_type": type(error).__name__
						}
				}
		}


class J2API(http.Controller):

		# post new data to model
		@http.route(
				'/api/<string:model>/',
				type='json', auth="public", methods=['POST'], csrf=False)
		def post_model_data(self, model, **post):
				try:
						data = post['data']
				except KeyError as e:
						msg = "`data` parameter is not found on POST request body"
						raise exceptions.ValidationError(msg)

				try:
						model_to_post = request.env[model]
				except KeyError as e:
						msg = "Model `%s` does not exist." % model
						raise exceptions.ValidationError(msg)

				record = model_to_post.create(data)
				return record.id


		# update material by id
		@http.route(
				'/api/<string:model>/<int:id>/',
				type='json', auth="public", methods=['PUT'], csrf=False)
		def put_model_record(self, model, id, **post):
				try:
						data = post['data']
				except KeyError:
						msg = "`data` parameter is not found on PUT request body"
						raise exceptions.ValidationError(msg)

				try:
						model_to_put = request.env[model]
				except KeyError:
						msg = "Model `%s` does not exist." % model
						raise exceptions.ValidationError(msg)

				rec = model_to_put.browse(id).ensure_one()

				try:
						return rec.write(data)
				except Exception as e:
						return False


		# delete material by id
		@http.route(
				'/api/<string:model>/<int:id>/',
				type='http', auth="public", methods=['DELETE'], csrf=False)
		def delete_model_record(self, model,  id, **post):
				try:
						model_to_del_rec = request.env[model]
				except KeyError as e:
						msg = "Model `%s` does not exist." % model
						raise exceptions.ValidationError(msg)

				rec = model_to_del_rec.browse(id).ensure_one()
				try:
						is_deleted = rec.unlink()
						res = {
								"result": is_deleted
						}
						return http.Response(
								json.dumps(res),
								status=200,
								mimetype='application/json'
						)
				except Exception as e:
						res = error_response(e, str(e))
						return http.Response(
								json.dumps(res),
								status=200,
								mimetype='application/json'
						)


		# get all data in model
		@http.route(
				'/api/<string:model>/',
				type='http', auth='public', methods=['GET'], csrf=False)

		def get_data(self, model):
				
				try:
						records = request.env[model].search([])
						data = []

						if model == 'j2_api.materials':

								for row in records:
										data.append({
												'id': row.id,
												'name': row.material_name,
												'code': row.material_code,
												'type': row.material_type,
												'price': row.material_buy_price
										})

						else:

								for row in records:
										data.append({
												'id': row.id,
												'name': row.supplier_name
										}) 
						
						result = {
								'data': data
						}
				except KeyError as e:
						res = error_response(e, e.msg)
						return http.Response(
								json.dumps(res),
								status=200,
								mimetype='application/json'
						)
				
				return http.Response(
						json.dumps(result),
						status=200,
						mimetype='application/json'
				)


		# get material by type
		@http.route(
				'/api/<string:model>/<string:search>',
				type='http', auth='public', methods=['GET'], csrf=False)

		def get_product_by_material(self, model, search):
				
				try:
						records = request.env[model].search([('material_type', '=', search)])
						data = []
						for row in records:
								data.append({
										'id': row.id,
										'name': row.material_name,
										'code': row.material_code,
										'type': row.material_type,
										'price': row.material_buy_price
								})        
						
						result = {
								'data': data
						}
				except KeyError as e:
						res = error_response(e, e.msg)
						return http.Response(
								json.dumps(res),
								status=200,
								mimetype='application/json'
						)
				
				return http.Response(
						json.dumps(result),
						status=200,
						mimetype='application/json'
				)

