{
		'name': "J2 REST API",

		'summary': """
				J2 REST API""",

		'description': """
				J2 REST API
		""",

		'author': "Wawan B. Setyawan",
		'website': "https://bitbucket.com/wawanbsetyawan/j2-rest-api",

		# Categories can be used to filter modules in modules listing
		'category': 'developers',
		'version': '0.1',

		# any module necessary for this one to work correctly
		'depends': ['base'],

		'data':[
				"security/ir.model.access.csv"
		],

		"application": True,
		"installable": True,
		"auto_install": False,

		'external_dependencies': {
				'python': []
		}
}